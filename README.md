# Cspell Checker

Docker images with cspell [https://github.com/streetsidesoftware/cspell](https://github.com/streetsidesoftware/cspell)

## Supported tags and respective Dockerfile links

- `4.1.0-node12-alpine`, `4.1.0`, `latest` [(4.1.0-node12-alpine/Dockerfile)](4.1.0-node12-alpine/Dockerfile)
